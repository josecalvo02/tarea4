package Poo;

import java.util.*;
import javax.swing.*;


public class GetInfo {
    
    public static void main (String[] args){
        ShowInfo show = new ShowInfo();
        UpdateInfo upt = new UpdateInfo();
        int loop = 0;
        int count = 0;
        
        // This array will have the information
        Person[] array = new Person [1];
                
        //This loop repeats the process until you choose to quit
        while(loop != -1){
            int index = 0;
            //System.out.println("Escriba el id del usuario del que desea agregar o modificar información o escriba el #2 para salir");
            int opt = Integer.parseInt(JOptionPane.showInputDialog("Escriba el id del usuario del que desea agregar o modificar información o escriba el #2 para salir"));
            
            if(opt == 2){
                loop = -1;
                System.out.println("Adios");
            }
            else{
                int num = Integer.parseInt(JOptionPane.showInputDialog("Digite\n1)Si desea agregar un nuevo usuario\n2)Mostrar los datos\n3)Modificar los datos"));
                switch (num) {
                    
                    //This case creates users
                    case 1:
                        
                        //This loop looks if there is already registered the user's id
                        if(array.length == 1){
                            
                            //These code lines adds the information to the profile
                            array[0]= new Person(opt);
                            array[0].setName(JOptionPane.showInputDialog("Digite el nombre"));
                            array[0].setLastName(JOptionPane.showInputDialog("Digite el apellido"));
                            array[0].setDateBirth(Integer.parseInt(JOptionPane.showInputDialog("Digite el día de nacimiento en numeros")));
                            array[0].setAccountNum(Long.parseLong(JOptionPane.showInputDialog("Digite el número de cuenta")));
                            array[0].setState(JOptionPane.showInputDialog("Digite el Estado"));
                            array[0].setCity(JOptionPane.showInputDialog("Digite la Cuidad"));
                            array[0].setCellphone(Integer.parseInt(JOptionPane.showInputDialog("Digite el numero de teléfono")));
                            array[0].setEmail(JOptionPane.showInputDialog("Digite el email"));
                            array[0].setAmount(Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad")));
                            
                            //This method adds a new space for an user
                            array = Arrays.copyOf(array,array.length+1);
                            
                            System.out.println("Se han guardado los datos con éxito");
                        }
                        else{
                            for (int i = 0; i < array.length-1; i++) {
                                System.out.println(i);
                                if(array[i].getId() == opt){
                                    count = -1;
                                    i = array.length+1;
                                }
                            }   
                            if(count == -1){
                                System.out.println("El nombre de usuario ya está registrado, vuelva a intentar");
                                count = 0;
                            }
                            else{
                                //This part adds the information to the profile
                                array[array.length-1]= new Person(opt);
                                array[array.length-1].setName(JOptionPane.showInputDialog("Digite el nombre"));
                                array[array.length-1].setLastName(JOptionPane.showInputDialog("Digite el apellido"));
                                array[array.length-1].setDateBirth(Integer.parseInt(JOptionPane.showInputDialog("Digite el día de nacimiento en numeros")));
                                array[array.length-1].setAccountNum(Long.parseLong(JOptionPane.showInputDialog("Digite el número de cuenta")));
                                array[array.length-1].setState(JOptionPane.showInputDialog("Digite el Estado"));
                                array[array.length-1].setCity(JOptionPane.showInputDialog("Digite la Cuidad"));
                                array[array.length-1].setCellphone(Integer.parseInt(JOptionPane.showInputDialog("Digite el numero de teléfono")));
                                array[array.length-1].setEmail(JOptionPane.showInputDialog("Digite el email"));
                                array[array.length-1].setAmount(Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad")));
                                
                                //This method adds a new space for an user
                                array = Arrays.copyOf(array,array.length+1);
                                
                                System.out.println("Se han guardado los datos con éxito");
                            }
                        }   
                        break;
                    
                    //This case shows info    
                    case 2:
                        if(array[0] == null){
                            System.out.println("No hay usuarios creados aun");
                        }
                        else{
                            for (int i = 0; i < array.length-1; i++){
                                if(array[i].getId() == opt){
                                    index = i;
                                    i = array.length+1;
                                }
                            }
                            show.showInfo(index, array);
                        }
                        break;
                    
                    //This case updates info
                    case 3:
                        if(array[0] == null){
                            System.out.println("No hay usuarios creados aun");
                        }
                        else{
                            for (int i = 0; i < array.length-1; i++){
                                if(array[i].getId() == opt){
                                    index = i;
                                    i = array.length+1;
                                }
                            }
                            int loopWhile = 1;
                            while(loopWhile != -1){
                                upt.updateInfo(index, array, loopWhile);
                                loopWhile = upt.updateInfo(index, array, loopWhile);
                            } 
                        }
                        break;
                        
                    default:
                        System.out.println("Numero no valido");
                        break;
                }
            }  
        }
    }   
}
