package Poo;

public class ShowInfo {
    
    public void showInfo(int index, Person array[]) {
        //This method shows information
        System.out.println("=====>  Información personal: <=====\n" +
        "Id: " + array[index].getId() + "\n" +
        "Nombre: " + array[index].getName() + "\n" +
        "Apellido: " + array[index].getLastName() + "\n" +
        "Fecha de nacimiento: " + array[index].getDateBirth() + "\n" +
        "Número de cuenta: " + array[index].getAccountNum() + "\n" +        
        "Estado: " + array[index].getState() + "\n" +
        "Cuidad: " + array[index].getCity() + "\n" +        
        "Número de teléfono: " + array[index].getCellphone() + "\n" +
        "Email: " + array[index].getEmail() + "\n" +        
        "Cantidad: " + array[index].getAmount());
    } 
}
