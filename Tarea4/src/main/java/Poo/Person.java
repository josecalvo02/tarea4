
package Poo;

public class Person {
    //Creating necessary vars
    private int id;
    private String name = "";
    private String lastName = "";
    private int dateBirth;
    private long accountNum;
    private String state = "";
    private String city = "";
    private int cellphone;
    private String email = "";
    private int amount;

    public Person(int id){
        this.id = id;
    }

    //These methods allows to add the information
    public void setId (int id){
        this.id = id;
    }
    public void setName (String name){
        this.name = name;
    }
    public void setLastName (String lastName){
        this.lastName = lastName;
    }
    public void setDateBirth (int dateBirth){
        this.dateBirth = dateBirth;
    }
    public void setAccountNum (long accountNum){
        this.accountNum = accountNum;
    }
    public void setState (String state){
        this.state = state;
    }
    public void setCity (String city){
        this.city = city;
    }
    public void setCellphone (int cellphone){
        this.cellphone = cellphone;
    }
    public void setEmail (String email){
        this.email = email;
    }
    public void setAmount (int amount){
        this.amount = amount;
    }
    
    //These methods allows to show the information
    public int getId(){
       return this.id;
    }
    public String getName(){
       return this.name;
    }
    public String getLastName(){
       return this.lastName;
    }
    public int getDateBirth(){
       return this.dateBirth;
    }
    public long getAccountNum(){
       return this.accountNum;
    }
    public String getState(){
       return this.state;
    }
    public String getCity(){
       return this.city;
    }
     public int getCellphone(){
       return this.cellphone;
    }
    public String getEmail(){
       return this.email;
    }
    public int getAmount(){
       return this.amount;
    }
}
